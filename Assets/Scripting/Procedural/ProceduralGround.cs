﻿using UnityEngine;

public class ProceduralGround : MonoBehaviour
{
	public Renderer obj;

	private void Start()
	{
		Texture2D proceduralTexture = CreateTexture(128, 128);
		proceduralTexture = ProcessTexture(proceduralTexture, 5); //error: reading and riding to the same texture -_-
		obj.material.mainTexture = proceduralTexture;
	}

	private void Update()
	{
		if (Input.GetKeyDown(KeyCode.Space))
		{
			Start();
		}
	}

	private Texture2D CreateTexture(int width, int height)
	{
		Texture2D texture = new Texture2D(width, height);
		texture.filterMode = FilterMode.Point;
		for (int x = 0; x < width; x++)
		{
			for (int y = 0; y < height; y++)
			{
				texture.SetPixel(x, y, new Color(0f, 0f, 0f, Random.value));
			}
		}
		texture.Apply();
		return texture;
	}

	private Texture2D ProcessTexture(Texture2D inputTexture, int blockRadius)
	{
		Texture2D processedTexture = new Texture2D(inputTexture.width, inputTexture.height);
		processedTexture.filterMode = FilterMode.Point;
		
		for (int x = 0; x < inputTexture.width; x++)
		{
			for (int y = 0; y < inputTexture.height; y++)
			{
				float sum = 0;
				//go around
				for (int offsetX = -blockRadius; offsetX <= blockRadius; offsetX++)
				{
					int pixelX = x + offsetX;
					if (CheckIsInBounds(pixelX, inputTexture.width))
					{
						for (int offsetY = -blockRadius; offsetY <= blockRadius; offsetY++)
						{
							int pixelY = y + offsetY;
							if (CheckIsInBounds(pixelY, inputTexture.height))
							{
								float pixelValue = inputTexture.GetPixel(pixelX, pixelY).a;
								if (pixelValue > 0.5f)
								{
									sum += 1f;
								}
								else if (pixelValue > 0.3f)
								{
									sum += 0.3f;
								}
							}
						}
					}
				}

				if (sum / blockRadius > 12.3f)
				{
					processedTexture.SetPixel(x, y, Color.black);
				}
				else
				{
					processedTexture.SetPixel(x, y, Color.white);
				}
			}
		}
		processedTexture.Apply();
		return processedTexture;
	}

	private bool CheckIsInBounds(int value, int max)
	{
		return value == Mathf.Clamp(value, 0, max);
	}
}