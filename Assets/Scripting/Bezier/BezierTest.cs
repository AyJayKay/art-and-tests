﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BezierTest : MonoBehaviour
{
	[Range(0f, 1f)] public float SinglePointerValue;
	public bool AnimateSinglePointer;
	public float AnimationSpeed;
	[Range(0f, 5f)] public float ErrorMultiply;
	public bool ShowSimpleBezierCurve;
	public int DrawSteps;
	[Range(0f, 1f)] public float Strength;
	public List<Transform> Nodes = new List<Transform>();

	private BezierGraph _bezierGraph;
	private Vector3 _oldPointerPosition = Vector3.zero;

	private void Update()
	{
		_bezierGraph = BezierGraphFactory.CreateBezierGraph(Nodes.Select(t => t.position).ToArray(), Strength);
		if (AnimateSinglePointer)
		{
			SinglePointerValue += AnimationSpeed * Time.deltaTime;
			if (SinglePointerValue >= 1)
			{
				SinglePointerValue = 0;
			}
		}
	}

	private void OnDrawGizmos()
	{
		if (_bezierGraph != null)
		{
			Vector3[] renderedPositions = new Vector3[DrawSteps + 1]; //with zero
			for (int i = 0; i <= DrawSteps; i++)
			{
				float current = (float) i / DrawSteps;
				Vector3 currentPosition = _bezierGraph.GetPosition(current);
				renderedPositions[i] = currentPosition;
			}

			float[] distances = new float[DrawSteps];
			float summedDistance = 0f;
			for (int i = 0; i < renderedPositions.Length - 1; i++)
			{
				distances[i] = Vector3.Distance(renderedPositions[i], renderedPositions[i + 1]);
				summedDistance += distances[i];
			}

			float avgDistance = summedDistance / distances.Length;

			for (int i = 0; i < _bezierGraph.NodesWithWeights.Length - 1; i++)
			{
				Gizmos.color = Color.magenta;
				Gizmos.DrawLine(_bezierGraph.NodesWithWeights[i], _bezierGraph.NodesWithWeights[i + 1]);
			}

			for (int i = 0; i < distances.Length; i++)
			{
				float error = (distances[i] - avgDistance) / avgDistance * ErrorMultiply;
				float greyValue = 0.5f + error;
				Gizmos.color = new Color(greyValue, greyValue, greyValue);
				Gizmos.DrawLine(renderedPositions[i], renderedPositions[i + 1]);
			}

			Vector3 pointerPosition = _bezierGraph.GetPosition(SinglePointerValue);
			Debug.Log("Pointer moved: " + Vector3.Distance(_oldPointerPosition, pointerPosition));
			Gizmos.color = Color.blue;
			Gizmos.DrawWireSphere(pointerPosition, 0.5f);
			_oldPointerPosition = pointerPosition;

			if (ShowSimpleBezierCurve)
			{
				Gizmos.color = Color.green;
				for (int i = 0; i < DrawSteps; i++)
				{
					Gizmos.DrawLine(
						BezierGraphFactory.CalcSimpleBezier(
							Nodes.Select(t => t.position).ToArray(),
							(float) i / DrawSteps
						),
						BezierGraphFactory.CalcSimpleBezier(
							Nodes.Select(t => t.position).ToArray(),
							(float) (i + 1) / DrawSteps
						)
					);
				}
			}
		}
	}
}