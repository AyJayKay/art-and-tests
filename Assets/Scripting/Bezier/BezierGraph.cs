﻿using System;
using UnityEngine;

public class BezierGraph
{
	public readonly Vector3[] NodesWithWeights;
	private readonly float _summendDistance;
	private const int NormalJumpSize = 3;
	private const int FirstAndLastJumpSize = 2;

	public BezierGraph(Vector3[] nodesWithWeights, float summendDistance)
	{
		BezierGraphFactory.ValidateNodes(nodesWithWeights);

		NodesWithWeights = nodesWithWeights;
		_summendDistance = summendDistance;
	}

	public Vector3 GetPosition(float value)
	{
		value = Mathf.Clamp01(value);

		float currentDistance = 0f;

		//iterate over small bezier sets
		int i = 0;
		while (i < NodesWithWeights.Length - 1)
		{
			//how many indices to the next start of small bezier curve
			//first and last small curves have less weights
			int jumpSize = NormalJumpSize;
			if (i == 0 || i == NodesWithWeights.Length - NormalJumpSize) jumpSize = FirstAndLastJumpSize;

			float distanceToNext = (NodesWithWeights[i + jumpSize] - NodesWithWeights[i]).magnitude;

			//if current small curve contains the wanted point
			if (currentDistance + distanceToNext >= _summendDistance * value)
			{
				//create a slice for small curve
				Vector3[] arraySlice = new Vector3[jumpSize + 1];
				Array.Copy(NodesWithWeights, i, arraySlice, 0, arraySlice.Length);

				/*
				Calculation of local value:
				
				first node                                        last node
				v                                                 v
				|--------------------|--------|                      <- 'currentDistance' + 'distanceToNext'
				|-------------------------------------------------|  <- 'summendDistance'
				|--------------------------|                         <- 'summendDistance' * 'value'
				                     |-----|                         <- this is the wanted length
				                                                        deviding this by 'distanceToNext'
				                                                        will give the local value
				                                                        (the bezier value for local curve)
				*/
				return BezierGraphFactory.CalcSimpleBezier(
					arraySlice,
					(_summendDistance * value - currentDistance) / distanceToNext
				);
			}

			currentDistance += distanceToNext;
			i += jumpSize;
		}
		//at the end return last position
		return NodesWithWeights[NodesWithWeights.Length - 1];
	}
}