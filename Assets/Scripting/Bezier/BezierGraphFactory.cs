﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class BezierGraphFactory
{
	public static BezierGraph CreateBezierGraph(Vector3[] nodes, float strength)
	{
		ValidateNodes(nodes);
		strength = Mathf.Clamp01(strength);

		List<Vector3> nodesAndWeights = new List<Vector3>();
		float summedDistance = 0f;

		//add to list
		nodesAndWeights.Add(nodes[0]);
		summedDistance += (nodes[1] - nodes[0]).magnitude; //add to distance from first to second
		//loop all except fist and last
		for (int i = 1; i < nodes.Length - 1; i++)
		{
			//add do distance from current to next
			summedDistance += (nodes[i + 1] - nodes[i]).magnitude;

			//build weights before and after
			Vector3 outerEdge = nodes[i - 1] - nodes[i + 1]; //from next node to previous node

			//calc fist weight
			Vector3 weightDir = outerEdge;
			//apply distance to "before weight" to half of distance to the "before node"
			weightDir = weightDir.normalized * (nodes[i - 1] - nodes[i]).magnitude * strength;
			Vector3 weightPoint = nodes[i] + weightDir; //and translate to get position

			nodesAndWeights.Add(weightPoint);

			//now add own point (it's between "before weight" and "after weight")
			nodesAndWeights.Add(nodes[i]);

			//calc second weight
			weightDir = -outerEdge; //negative duo to other direction, now
			//apply distance to "after weight" to half of distance to the "after node"
			weightDir = weightDir.normalized * (nodes[i] - nodes[i + 1]).magnitude * strength;
			weightPoint = nodes[i] + weightDir; //apply this one too

			nodesAndWeights.Add(weightPoint);
		}
		nodesAndWeights.Add(nodes[nodes.Length - 1]);

		return new BezierGraph(nodesAndWeights.ToArray(), summedDistance);
	}

	public static Vector3 CalcSimpleBezier(Vector3[] nodes, float value)
	{
		while (nodes.Length > 1)
		{
			Vector3[] newNodes = new Vector3[nodes.Length - 1];
			for (int i = 0; i < nodes.Length - 1; i++)
			{
				Vector3 rayToNext = nodes[i + 1] - nodes[i];
				rayToNext *= value;
				Vector3 translatedPoint = nodes[i] + rayToNext;

				newNodes[i] = translatedPoint;
			}
			nodes = newNodes;
		}

		return nodes[0];
	}

	public static void ValidateNodes(Vector3[] nodes)
	{
		if (nodes == null)
			throw new NullReferenceException("The list of nodes is NULL and can't be used!");
		if (nodes.Length <= 1)
			throw new NodesNotValidException("Not enough nodes to create a graph!");
	}
}

internal class NodesNotValidException : Exception
{
	public NodesNotValidException(string message) : base(message)
	{
	}
}