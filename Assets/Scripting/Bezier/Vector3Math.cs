﻿using UnityEngine;

public class Vector3Math
{
	public static Vector3 CenterOf(Vector3 first, Vector3 second)
	{
		return (second - first) * 0.5f + first;
	}
}