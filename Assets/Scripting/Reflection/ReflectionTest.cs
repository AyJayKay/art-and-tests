﻿using System.Reflection;
using UnityEngine;

public class ReflectionTest : MonoBehaviour
{
	void Start()
	{
		TestClass c1 = new TestClass();
		TestClass c2 = new TestClass();
		string replace = "Hallo!";

		FieldInfo[] testClassFields =
			typeof(TestClass).GetFields(BindingFlags.NonPublic | BindingFlags.Public |
			                            BindingFlags.Instance | BindingFlags.Static);
		if (testClassFields.Length > 0)
		{
			string classInfo = "Fields in Class:";
			foreach (FieldInfo field in testClassFields)
			{
				classInfo += "\n" +
				             (field.IsPublic ? "public " : "") +
				             (field.IsPrivate ? "private " : "") +
				             (field.IsStatic ? "static " : "") +
				             field.FieldType + " " + field.Name;
			}
			Debug.Log(classInfo);

			//set new value of c2
			testClassFields[0].SetValue(c2, replace);
		}

		Debug.Log("c1 value: " + c1.MyValue + " should be \"Default\"");
		Debug.Log("c2 value: " + c2.MyValue + " should be \"" + replace + "\"");
	}
}

public class TestClass
{
	private string myValue = "Default";
	private static string abc = "";
	public int i = 0;

	public string MyValue
	{
		get { return myValue; }
	}
}