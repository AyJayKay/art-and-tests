﻿using System;
using System.Collections;
using UnityEngine;

/*
Look at output. Called Dispose() multiple times to point out DestroyImmediate (second creation/destruction)
*/

public class CleanTest : MonoBehaviour
{
	private GameObject myCube;
	private MyComponent myComponent;

//	private void Start()
//	{
//		myCube = GameObject.CreatePrimitive(PrimitiveType.Cube);
//		myComponent = myCube.AddComponent<MyComponent>();
//		myComponent.Signifier = "One";
//
//		myComponent.Dispose();
//		Destroy(myCube);
//		myComponent.Dispose();
//		//myComponent = null; //gets overwritten, so old reference is lost => GC will collect it
//
//		//try detecting destroyed
//		Debug.Log("Is Null in same frame? " + (myComponent == null));
//		Debug.Log("Equals Null in same frame? " + myComponent.Equals(null));
//
//		Debug.Log("----------");
//
//		myCube = GameObject.CreatePrimitive(PrimitiveType.Cube);
//		myComponent = myCube.AddComponent<MyComponent>();
//		myComponent.Signifier = "Two";
//
//		myComponent.Dispose();
//		DestroyImmediate(myCube);
//		myComponent.Dispose();
//		//myComponent = null; //commented out: will not call Destructor (not free for GC)
//
//		//try detecting destroyed
//		Debug.Log("Is Null in same frame? " + (myComponent == null));
//		Debug.Log("Equals Null in same frame? " + myComponent.Equals(null));
//		try
//		{
//			myComponent.Signifier = "";
//		}
//		catch (MissingReferenceException e)
//		{
//			Debug.Log(e);
//		}
//
//		Debug.Log("----------");
//	}
	
	private void Start()
	{
		myCube = GameObject.CreatePrimitive(PrimitiveType.Cube);
		myComponent = myCube.AddComponent<MyComponent>();
		myComponent.Signifier = "One";

		Debug.Log(myComponent.GetInstanceID());
		DestroyImmediate(myCube);
		myCube = null;
//		GC.Collect();
//		GC.WaitForPendingFinalizers();

		StartCoroutine(Blubb());
	}

	private IEnumerator Blubb()
	{
		yield return null;
		yield return null;
		yield return null;
		Debug.Log(myComponent == null);
		Debug.Log(myComponent.Signifier);
		/*
		  ?  O_o  ?
		This will will log:
		"True"
		"One"
		*/
	}
}