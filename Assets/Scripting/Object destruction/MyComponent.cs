﻿using System;
using UnityEngine;

public class MyComponent : MonoBehaviour, IDisposable
{
	public string Signifier = "";

	public void Dispose()
	{
		Debug.Log("Dispose " + Signifier);
	}

	~MyComponent()
	{
		Debug.Log("destructor " + Signifier);
	}

	private void OnDisable()
	{
		Debug.Log("OnDisable " + Signifier);
	}

	private void OnDestroy()
	{
		Debug.Log("OnDestroy  " + Signifier);
	}
}