using System;
using UnityEngine;

public static class TriangleMath
{
	public static bool IsPointInTriangle(Vector2[] triangle, Vector2 point)
	{
		ValidateTriangle(triangle);

		float areaSum = 0;
		for (int i = 0; i < triangle.Length; i++)
		{
			int nextIndex = (i + 1) % triangle.Length;
			areaSum += CalculateTriangleArea(new[]
			{
				triangle[i],
				triangle[nextIndex],
				point
			});
		}

		return Math.Abs(areaSum - CalculateTriangleArea(triangle)) < 0.001f;
	}

	public static float CalculateTriangleArea(Vector2[] triangle)
	{
		ValidateTriangle(triangle);

		return Math.Abs(
			       triangle[0].x * (triangle[1].y - triangle[2].y)
			       + triangle[1].x * (triangle[2].y - triangle[0].y)
			       + triangle[2].x * (triangle[0].y - triangle[1].y)
		       ) / 2f;
	}

	private static void ValidateTriangle(Vector2[] triangle)
	{
		if (triangle.Length != 3)
		{
			throw new Exception("List representing the triangle doesn't hold three points.");
		}
	}

	public static Vector2 FindClosestPointOnTriangle(Vector2[] triangle, Vector2 point)
	{
		ValidateTriangle(triangle);

		if (IsPointInTriangle(triangle, point))
		{
			return point;
		}

		Vector2[] edgePoints = new Vector2[3];

		for (int i = 0; i < triangle.Length; i++)
		{
			int nextIndex = (i + 1) % triangle.Length;

			edgePoints[i] = FindClosestPointOnLine(triangle[i], triangle[nextIndex], point);
		}

		Vector2 closestPointOnEdge = edgePoints[0];
		for (var i = 1; i < edgePoints.Length; i++)
		{
			if (Vector2.Distance(edgePoints[i], point) < Vector2.Distance(closestPointOnEdge, point))
			{
				closestPointOnEdge = edgePoints[i];
			}
		}

		return closestPointOnEdge;
	}

	public static Vector2 FindClosestPointOnLine(Vector2 lineStart, Vector2 lineEnd, Vector2 point)
	{
		Vector3 normalizedProjection = Vector3.Project(point - lineStart, lineEnd - lineStart);

		Vector2 closestPoint = new Vector2(normalizedProjection.x, normalizedProjection.y) + lineStart;

		float lineLength = Vector2.Distance(lineStart, lineEnd);

		//check for "over shoot" of projection to clamp point on the line
		if (Vector2.Distance(closestPoint, lineStart) + Vector2.Distance(closestPoint, lineEnd) > lineLength + 0.001f)
		{
			if (Vector2.Distance(point, lineStart) < Vector2.Distance(point, lineEnd))
			{
				closestPoint = lineStart;
			}
			else
			{
				closestPoint = lineEnd;
			}
		}

		return closestPoint;
	}

	public static Vector2 LineLineIntersection(
		Vector2 linePoint1,
		Vector2 lineVec1,
		Vector2 linePoint2,
		Vector2 lineVec2
	)
	{
		Vector3 lineVec1In3D = new Vector3(lineVec1.x, lineVec1.y, 0f);
		Vector3 lineVec2In3D = new Vector3(lineVec2.x, lineVec2.y, 0f);
		
		Vector3 lineVec3 = linePoint2 - linePoint1;
		Vector3 crossVec1And2 = Vector3.Cross(lineVec1In3D, lineVec2In3D);
		Vector3 crossVec3And2 = Vector3.Cross(lineVec3, lineVec2In3D);

		float s = Vector3.Dot(crossVec3And2, crossVec1And2) / crossVec1And2.sqrMagnitude;
		return linePoint1 + (lineVec1 * s);
	}
}