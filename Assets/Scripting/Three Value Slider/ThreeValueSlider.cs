﻿using System;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

[RequireComponent(typeof(RectTransform))]
public class ThreeValueSlider : MonoBehaviour
{
	public float valueTop { get; private set; }
	public float valueRight { get; private set; }
	public float valueLeft { get; private set; }
	private RectTransform rectTransform;
	private Vector2[] triangle;
	private Vector2 topMaxVector;
	private Vector2 rightMaxVector;
	private Vector2 leftMaxVector;
	private float triangleHeight;
	public Button button;

	private void Start()
	{
		InitializeSlider();
		SetUpBounds();
	}

	private void Update()
	{
		Vector2 localPoint;
		if (
			Input.GetMouseButton(0)
			&& RectTransformUtility.ScreenPointToLocalPointInRectangle(
				rectTransform, Input.mousePosition, Camera.main,
				out localPoint
			))
		{
			localPoint = TriangleMath.FindClosestPointOnTriangle(triangle, localPoint);
			button.GetComponent<RectTransform>().anchoredPosition = localPoint;
			CalculateValues(localPoint);
		}

		if (Input.GetKeyDown(KeyCode.W)) CalculatePosition(0, 1, 0);
		if (Input.GetKeyDown(KeyCode.A)) CalculatePosition(1, 0, 0);
		if (Input.GetKeyDown(KeyCode.D)) CalculatePosition(0, 0, 1);
		if (Input.GetKeyDown(KeyCode.E)) CalculatePosition(0, 1, 1);
		if (Input.GetKeyDown(KeyCode.Q)) CalculatePosition(1, 1, 0);
		if (Input.GetKeyDown(KeyCode.S)) CalculatePosition(1, 0, 1);
		if (Input.GetKeyDown(KeyCode.Space))
		{
			float top = Random.value;
			float right = Random.value;
			float left = Random.value;
			Debug.Log(String.Format("left: {0}, top: {1}, right: {2}", left, top, right));
			CalculatePosition(left, top, right);
		}
	}

	private void OnGUI()
	{
		GUI.Label(new Rect(0, 0, 100, 50), "Top: " + valueTop);
		GUI.Label(new Rect(0, 50, 100, 50), "Right: " + valueRight);
		GUI.Label(new Rect(0, 100, 100, 50), "Left: " + valueLeft);
	}

	public void InitializeSlider()
	{
		rectTransform = GetComponent<RectTransform>();

		float height = Mathf.Sqrt(3f) / 2f;
		triangle = new[]
			{
				new Vector2(.5f, height),
				Vector2.right,
				Vector2.zero
			}
			.Select(v => new Vector2(v.x * rectTransform.rect.width, v.y * rectTransform.rect.height))
			.ToArray();

//		float halfBaseLine = 1f / Mathf.Sqrt(3f);
//		triangle = new[]
//			{
//				new Vector2(0f, 1f),
//				new Vector2(halfBaseLine, 0f),
//				new Vector2(-halfBaseLine, 0f)
//			}
//			.Select(v => new Vector2(v.x * rectTransform.rect.width, (v.y - .5f) * rectTransform.rect.height)).ToArray();

		triangleHeight = height * rectTransform.rect.height;
		topMaxVector = (triangle[1] + triangle[2]) / 2f - triangle[0];
		rightMaxVector = (triangle[2] + triangle[0]) / 2f - triangle[1];
		leftMaxVector = (triangle[0] + triangle[1]) / 2f - triangle[2];
	}

	private void SetUpBounds()
	{
		for (int i = 0; i < triangle.Length; i++)
		{
			Instantiate(button, rectTransform).GetComponent<RectTransform>().anchoredPosition = triangle[i];
		}
	}

	private void CalculateValues(Vector2 localPoint)
	{
		valueTop = 1f - (Vector3.Project(localPoint - triangle[0], topMaxVector).magnitude / triangleHeight);
		valueRight = 1f - (Vector3.Project(localPoint - triangle[1], rightMaxVector).magnitude / triangleHeight);
		valueLeft = 1f - (Vector3.Project(localPoint - triangle[2], leftMaxVector).magnitude / triangleHeight);

		float sum = valueTop + valueRight + valueLeft;
		valueTop = valueTop / sum;
		valueRight = valueRight / sum;
		valueLeft = valueLeft / sum;
	}

	private Vector2 CalculatePosition(float leftValue, float topValue, float rightValue)
	{
		float sum = topValue + rightValue + leftValue;
		valueTop = topValue / sum;
		valueRight = rightValue / sum;
		valueLeft = leftValue / sum;

		Vector2 localPoint = TriangleMath.LineLineIntersection(
			-topMaxVector * valueTop,
			triangle[1] - triangle[2],
			-rightMaxVector * valueRight,
			triangle[0] - triangle[2]
		);

		button.GetComponent<RectTransform>().anchoredPosition = localPoint;
		return localPoint;
	}
}