﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/CutoutMobHLSL" {
   Properties {
    _Color ("Main Color", Color) = (1,1,1,1)
    _MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}
    _CutTex ("Cutout (A)", 2D) = "white" {}
    _CutOffStart ("Alpha cutoff start", Range(0.0,1.0)) = 0.0
    _CutOffEnd ("Alpha cutoff end", Range(0.0,1.0)) = 1.0
    [MaterialToggle] _Invert ("_Invert cutoff", float) = 0
   }
   SubShader {
    Tags { "Queue" = "Transparent" }
    Pass {
     ZWrite On
     //AlphaToMask On
     Blend SrcAlpha OneMinusSrcAlpha // use alpha blending
     CGPROGRAM
         #pragma vertex vert
         #pragma fragment frag
         
         uniform float4 _Color; // define shader property for shaders
         uniform sampler2D _MainTex;
         uniform sampler2D _CutTex;
         uniform float _CutOffStart;
         uniform float _CutOffEnd;
         uniform float _Invert;
         
         struct vertexInput {
          float4 vertex : POSITION;
          float4 texcoord : TEXCOORD0;
         };
         struct vertexOutput {
          float4 pos : SV_POSITION;
          float4 tex : TEXCOORD0;
         };
     
         vertexOutput vert(vertexInput input) {
          vertexOutput output;
     
          output.tex = input.texcoord;
          output.pos = UnityObjectToClipPos(input.vertex);
          return output;
         }
         
         float4 frag(vertexOutput input) : COLOR {
          float2 tp = float2(input.tex.x, input.tex.y); //get textures coordinate
          float4 col = tex2D(_MainTex, tp) * _Color; //load main texture
          float opacity = tex2D(_CutTex, tp).a; //load cuttext
          if (
            (_Invert == 0 && (_CutOffStart >= _CutOffEnd || opacity < _CutOffStart || opacity > _CutOffEnd))
            ||
            (_Invert == 1 && (opacity >= _CutOffStart && opacity <= _CutOffEnd))
          ) {
            return float4(col.r, col.g, col.b, 0.0);
          }
          else {
            return col;
          }
         }
     ENDCG
    }
   }
  }