﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

public class LoadingCubeSpell : MonoBehaviour
{
	public GameObject prefab;
	public string materialValue;
	public int maximumValue = 1;
	public float speed = 1f;
	private List<GameObject> currentCubes = new List<GameObject>();
	public int waitingToLoad = 0;
	private GameObject currentLoadingCube;

	void Update()
	{
		if (!currentLoadingCube && waitingToLoad > 0)
		{
			waitingToLoad--;
			currentLoadingCube = GameObject.Instantiate(prefab, transform) as GameObject;
			currentLoadingCube.transform.localScale = Vector3.one * 0.3f;
		}
		else if (currentLoadingCube)
		{
			if (currentCubes.Count < maximumValue)
			{
				currentLoadingCube.transform.localPosition = Vector3.MoveTowards(currentLoadingCube.transform.localPosition, Vector3.zero, Time.deltaTime * speed);
				currentLoadingCube.transform.localScale = Vector3.MoveTowards(currentLoadingCube.transform.localScale, Vector3.one, Time.deltaTime * speed);

				if (currentLoadingCube.transform.localScale.x > 0.9999f)
				{
					currentLoadingCube.transform.localScale = Vector3.one;
					currentLoadingCube.transform.localPosition = Vector3.zero;
					currentCubes.Add(currentLoadingCube);
					currentLoadingCube = null;
				}
			}
			else
			{
				currentLoadingCube.transform.localPosition = Vector3.Lerp(currentLoadingCube.transform.localPosition, new Vector3(0f, 1.6f, 0f), Time.deltaTime * speed);
				if (currentLoadingCube.transform.localPosition.y > 1.3f)
				{
					Destroy(currentLoadingCube);
					currentLoadingCube = null;
				}
			}
		}
	}

	public void Fire(Vector3 target)
	{
		if (currentCubes.Count > 0)
		{
			
		}
	}

	public void Load(int amount)
	{
		waitingToLoad += amount;
	}
}
